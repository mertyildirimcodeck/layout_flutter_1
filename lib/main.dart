import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          primarySwatch: Colors.orange,
          accentColor: Colors.orange,
          textTheme: const TextTheme(
              bodyText2: TextStyle(
            fontSize: 24,
            fontStyle: FontStyle.italic,
          ))),
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Building Layouts with Flutter'),
        ),
        body: Row(
          children: [
            Container(
              margin: const EdgeInsets.all(10.0),
              width: 100,
              height: 100,
              decoration: const BoxDecoration(
                color: Colors.deepPurpleAccent,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.all(
                  Radius.elliptical(5, 10),
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.all(10.0),
              width: 100,
              height: 100,
              decoration: const BoxDecoration(
                color: Colors.red,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.all(10.0),
              width: 100,
              height: 100,
              decoration: BoxDecoration(
                image: const DecorationImage(
                  image: NetworkImage(
                    'https://static.wikia.nocookie.net/dota2_gamepedia/images/4/4d/Team_logo_Team_Spirit.png/revision/latest?cb=20210528035742',
                  ),
                ),
                color: Colors.red,
                gradient: LinearGradient(
                  //tileMode: TileMode.repeated,
                  //begin: Alignment.topCenter,
                  begin: const Alignment(0.0, -0.3),
                  //end: Alignment.bottomCenter,
                  end: const Alignment(0.0, 1.0),
                  colors: [
                    Colors.purple.shade50,
                    Colors.purple.shade700,
                  ],
                ),
                shape: BoxShape.rectangle,
                borderRadius: const BorderRadius.all(
                  Radius.circular(10),
                ),
              ),
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.lightbulb_outline),
          onPressed: () {},
        ),
        persistentFooterButtons: <Widget>[
          IconButton(
            onPressed: () {},
            icon: const Icon(Icons.add_comment),
          ),
          IconButton(
            onPressed: () {},
            icon: const Icon(Icons.add_alarm),
          ),
          IconButton(
            onPressed: () {},
            icon: const Icon(Icons.add_location),
          ),
        ],
      ),
    );
  }
}
